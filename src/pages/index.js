import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import styled from 'styled-components';
import logoMain from '../images/bitmap.png';
import logoServicios from '../images/homeServicios.jpg';

const servicioText = 'Tenemos servicios de alto nivel, cortes de cabello, cuidado de la barba, afeitado toallas calientes, masajes y cuidado de la piel. Estos son algunos de nuestros servicios más solicitados';

const Button = styled.button`
  width: 150px;
  height: 50px;
  background: #67666A;
  border: 0;
  color: #fff;
  font-family: 'helvetica';
  font-size: 16px;
  margin-top: 50px;
`;

const ReservaButton = styled(Button)`
  background: #B89E81;
  color: #414141;
  font-weight: 600;
`;

const Container = styled.div`
  margin-left: 10vw;
  margin-top: 25vh;
  width:360px;
  height:420px;
  background-color: #D8D8D8;
  display:grid;
  grid-template-columns: 180px 180px;
  grid-row: auto auto;
  grid-column-gap: 2px;
  grid-row-gap: 2px;
`;

const Box = styled.div`
  background-color: #fff;
  padding-top: 20px;
  padding: 10px;
  border-radius: 0px;
  color: #626262;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  font-family: arvo;
  flex-direction: column;
`;

const serviciosLocal = [
  {
    id: 1,
    nombreServicio: 'Corte + Lavado',
    descServicio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    img: 'servicioCorte'
  },
  {
    id: 2,
    nombreServicio: 'Corte + Perfilado',
    descServicio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    img: 'servicioPerfilado'
  },
  {
    id: 3,
    nombreServicio: 'Peinado',
    descServicio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    img: 'servicioPeinado'
  },
  {
    id: 4,
    nombreServicio: 'Afeitado',
    descServicio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    img: 'servicioAfeitado'
  }
];

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      {/* First View - Home */}
      <div style={{ width: window.innerWidth, height: '88vh', display: 'block', position: 'relative', overflow: 'hidden', backgroundColor: '#05101C', }}>
        <img style={{ position: 'absolute', width: '85%', }} src={logoMain} alt={"logoE26"} />
        <div style={{ position: 'relative', top: '25vh', left: '25vw',alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
          <h3 style={{ color: '#fff', fontFamily: 'arvo', fontWeight: 'bold', fontSize: 40 }}>Estudio 26</h3>
          <p style={{ textAlign: 'center', width: '300px' ,color: '#fff', fontFamily: 'helvetica', wordWrap: 'break-word'}}>Consectetur adipiscing elit. Mauris in cursus justo, non faucibus enim. Nunc molestie nibh quis nunc </p>
          <Button>Reserva Ahora</Button>
        </div>
      </div>

      {/* Second View - Services */}
      <div style={{ width: window.innerWidth, height: '100vh', display: 'block', position: 'relative', overflow: 'hidden', backgroundColor: '#fff', }}>
        <img style={{ position: 'absolute', width: '65%', alignSelf: 'center' }} src={logoServicios} alt={"logoServicios"} />
        <div style={{ display: 'flex', flexDirection: 'row', position: 'absolute', alignSelf: 'center' }}>
          <div style={{ marginLeft: '25vw', marginTop: '30vh',display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <h1 style={{ textAlign: 'center', fontFamily: 'arvo', fontWeight: 'bold', fontSize: '30px', color: '#646464' }}>Nuestros Servicios</h1>
            <h1 style={{ letterSpacing: 1, fontWeight: 500 ,textAlign: 'justify', fontFamily: 'arvo', wordWrap: 'break-word', width: '300px', fontSize: '14px', color: '#626262' }}>
              {servicioText}
            </h1>
            <ReservaButton>Reservar</ReservaButton>
          </div>
          {/* Grid for services - second view */}
          <Container>
            {
              serviciosLocal.map((x, i) => {
                return(
                  <Box key={i}>
                    <img src={require(`../images/icons/${x.img}.png`)} alt={x.nombreServicio} style={{ width: '40%' }}/>
                    <h4>{x.nombreServicio}</h4>
                    <p style={{ textAlign: 'center', fontSize: 12, lineHeight: '1rem' }}>{x.descServicio}</p>
                  </Box>
                )
              })
            }
          </Container>
        </div>
      </div>
  </Layout>
)

export default IndexPage
