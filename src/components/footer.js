import React from "react"
import styled from 'styled-components'


const Footer = () => (
    <div style={{ backgroundColor: '#FAFAFA', bottom: 0, display: 'flex', flexDirection: 'row', height: '10rem', width: '100%', textAlign: 'center', justifyContent: 'center', fontFamily: 'arvo' }}>
        <div style={{ backgroundColor: 'red', display: 'flex', flexDirection: 'column', width: '100%' }}>
            <h3 style={{ fontFamily: 'arvo', fontSize: 16, marginTop: '5%' }}>Horario de Atención</h3>
        </div>
        <div style={{ backgroundColor: 'green', display: 'flex', flexDirection: 'column', width: '100%' }}>
            <h3 style={{ fontFamily: 'arvo', fontSize: 16, marginTop: '5%' }}>Dirección</h3>
        </div>
        <div style={{ backgroundColor: 'blue', display: 'flex', flexDirection: 'column', width: '100%' }}>
            <h3 style={{ fontFamily: 'arvo', fontSize: 16, marginTop: '5%' }}>Contacto</h3>
        </div>
    </div>
);

export default Footer
