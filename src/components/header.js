import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from 'styled-components'


const Label = styled.h3`
  margin: 0;
  color: #979797;
  font-family: 'arvo';
  font-weight: 'bold';
  font-size: 16px;
  text-align: justify;
`;

const Header = ({ siteTitle }) => (
  <header style={{ background: `white`, padding: `0.3rem`, }}>
    <div style={{ margin: 'auto', maxWidth: 960, display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center'}}>
      <Link to="/" style={{ textDecoration: `none` }}>
        <Label>Inicio</Label>
      </Link>      
      <Link to="/" style={{ textDecoration: `none` }}>
        <Label>Servicios</Label>
      </Link>            
      <Link to="/" style={{ textDecoration: `none` }}>
        <img style={{ margin:0 }} src={require('../images/icons/LOGO-E26.png')} alt={"logoE26"} width="70" height="70" />
      </Link>
      <Link to="/" style={{ textDecoration: `none` }}>
        <Label>Barberos</Label>
      </Link>      
      <Link to="/" style={{ textDecoration: `none` }}>
        <Label>Reserva</Label>
      </Link>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
